/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicus_v3;

//import java.util.Date;
import java.sql.Date;
/**
 *
 * @author Asus
 */
public class Comitet {
    private int idComitet;
    private Date dataInceput;
    private Date dataSfarsit;
    private String scop;
    public Comitet(int idComitet,Date dataInceput,Date dataSfarsit,String scop) {
        this.idComitet = idComitet;
        this.dataInceput = dataInceput;
        this.dataSfarsit = dataSfarsit;
        this.scop = scop;
    }

    /**
     * @return the idComitet
     */
    public int getIdComitet() {
        return idComitet;
    }

    /**
     * @param idComitet the idComitet to set
     */
    public void setIdComitet(int idComitet) {
        this.idComitet = idComitet;
    }

    /**
     * @return the dataInceput
     */
    public Date getDataInceput() {
        return dataInceput;
    }

    /**
     * @param dataInceput the dataInceput to set
     */
    public void setDataInceput(Date dataInceput) {
        this.dataInceput = dataInceput;
    }

    /**
     * @return the dataSfarsit
     */
    public Date getDataSfarsit() {
        return dataSfarsit;
    }

    /**
     * @param dataSfarsit the dataSfarsit to set
     */
    public void setDataSfarsit(Date dataSfarsit) {
        this.dataSfarsit = dataSfarsit;
    }

    /**
     * @return the scop
     */
    public String getScop() {
        return scop;
    }

    /**
     * @param scop the scop to set
     */
    public void setScop(String scop) {
        this.scop = scop;
    }
    
    
    
    
}
