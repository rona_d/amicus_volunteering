/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicus_v3;

import java.sql.Date;

public class Proiect {
    private int idProiect;
    private int idDepartament;
    private int idLocatie;
    private String numeProiect;
    private String descriereProiect;
    private Date dataInceput;
    private Date dataSfarsit;
    private int idComitet;
    
    
    
    public Proiect(int idProiect,int idDepartament,int idLocatie,String numeProiect,String descriereProiect,Date dataInceput,Date dataSfarsit,int idComitet)
    {
        this.idProiect = idProiect;
        this.idDepartament = idDepartament;
        this.idLocatie = idLocatie;
        this.numeProiect = numeProiect;
        this.descriereProiect = descriereProiect;
        this.dataInceput = dataInceput;
        this.dataSfarsit = dataSfarsit;
        this.idComitet = idComitet;
    }
    
    
    
    
    /**
     * @return the idProiect
     */
    public int getIdProiect() {
        return idProiect;
    }

    /**
     * @param idProiect the idProiect to set
     */
    public void setIdProiect(int idProiect) {
        this.idProiect = idProiect;
    }

    /**
     * @return the idDepartament
     */
    public int getIdDepartament() {
        return idDepartament;
    }

    /**
     * @param idDepartament the idDepartament to set
     */
    public void setIdDepartament(int idDepartament) {
        this.idDepartament = idDepartament;
    }

    /**
     * @return the idLocatie
     */
    public int getIdLocatie() {
        return idLocatie;
    }

    /**
     * @param idLocatie the idLocatie to set
     */
    public void setIdLocatie(int idLocatie) {
        this.idLocatie = idLocatie;
    }

    /**
     * @return the numeProiect
     */
    public String getNumeProiect() {
        return numeProiect;
    }

    /**
     * @param numeProiect the numeProiect to set
     */
    public void setNumeProiect(String numeProiect) {
        this.numeProiect = numeProiect;
    }

    /**
     * @return the dataInceput
     */
    public Date getDataInceput() {
        return dataInceput;
    }

    /**
     * @param dataInceput the dataInceput to set
     */
    public void setDataInceput(Date dataInceput) {
        this.dataInceput = dataInceput;
    }

    /**
     * @return the dataSfarsit
     */
    public Date getDataSfarsit() {
        return dataSfarsit;
    }

    /**
     * @param dataSfarsit the dataSfarsit to set
     */
    public void setDataSfarsit(Date dataSfarsit) {
        this.dataSfarsit = dataSfarsit;
    }

    /**
     * @return the idComitet
     */
    public int getIdComitet() {
        return idComitet;
    }

    /**
     * @param idComitet the idComitet to set
     */
    public void setIdComitet(int idComitet) {
        this.idComitet = idComitet;
    }

    /**
     * @return the descriereProiect
     */
    public String getDescriereProiect() {
        return descriereProiect;
    }

    /**
     * @param descriereProiect the descriereProiect to set
     */
    public void setDescriereProiect(String descriereProiect) {
        this.descriereProiect = descriereProiect;
    }
    
    
}
