/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicus_v3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Asus
 */
public class LoginForm extends javax.swing.JFrame {

    public static String userName;

    /**
     * Creates new form LoginForm
     */
    public LoginForm() {
        super("Log in");
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        userNameText = new javax.swing.JTextField();
        login = new javax.swing.JButton();
        back = new javax.swing.JButton();
        passWordText = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setSize(new java.awt.Dimension(800, 600));

        jPanel1.setBackground(new java.awt.Color(102, 0, 0));

        jPanel2.setBackground(new java.awt.Color(102, 0, 0));

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("HelvLight", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("UserName");

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setFont(new java.awt.Font("HelvLight", 1, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("PassWord");

        userNameText.setColumns(20);
        userNameText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                userNameTextActionPerformed(evt);
            }
        });

        login.setText("Login");
        login.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginActionPerformed(evt);
            }
        });

        back.setText("Back");
        back.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backActionPerformed(evt);
            }
        });

        passWordText.setColumns(20);
        passWordText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passWordTextActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(back))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(userNameText)
                            .addComponent(passWordText)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(120, 120, 120)
                        .addComponent(login)))
                .addGap(253, 253, 253))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(96, 96, 96)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(userNameText, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(passWordText, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 56, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(back)
                    .addComponent(login))
                .addGap(46, 46, 46))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 639, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void backActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backActionPerformed
        // TODO add your handling code here:
        this.setVisible(false);
        ChooseForm form = new ChooseForm();
        form.setVisible(true);
    }//GEN-LAST:event_backActionPerformed

    private void loginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginActionPerformed
        //Connection  conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/amicus?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root","");
            String sql = "select idUser,username,parola,tipUser from user where username = ? and parola = ?";
            PreparedStatement st = conn.prepareStatement(sql);

            st.setString(1, userNameText.getText());
            st.setString(2, passWordText.getText());
            userName = userNameText.getText();

            ResultSet rs = st.executeQuery();

            System.out.println("DONE");

            if (rs.next()) {
                String tip = rs.getString("tipUser");
                //System.out.println(tip);
                if (tip.equals("voluntarAmicus")) {
                    this.setVisible(false);

                    String SQL = "select idVoluntar from voluntaramicus amicus where idVoluntar = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        String sqlVol = "update user set enabled=1 where idUser = ?";
                        PreparedStatement stVol = conn.prepareStatement(sqlVol);
                        stVol.setInt(1, id);
                        stVol.execute();

                        JOptionPane.showMessageDialog(null, "Welcome voluntar!");
                        VoluntarMenu menu = new VoluntarMenu();
                        menu.setVisible(true);
                    }

                }

                if (tip.equals("sponsor")) {
                    this.setVisible(false);

                    String SQL = "select idSponsor from sponsor  where idSponsor = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        try {
                            String sqlSponsor = "update user set enabled=1 where idUser = ?";
                            PreparedStatement stSponsor = conn.prepareStatement(sqlSponsor);
                            stSponsor.setInt(1, id);
                            stSponsor.execute();

                            JOptionPane.showMessageDialog(null, "Welcome sponsor!");

                            SponsorMenu menu = new SponsorMenu();
                            menu.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                if (tip.equals("administrator")) {
                    this.setVisible(false);

                    String SQL = "select idAdmin from administrator  where idAdmin = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        try {
                            String sqlAdmin = "update user set enabled=1 where idUser = ?";
                            PreparedStatement stAdmin = conn.prepareStatement(sqlAdmin);
                            stAdmin.setInt(1, id);
                            stAdmin.execute();

                            JOptionPane.showMessageDialog(null, "Welcome admin!");

                            AdminMenu menu = new AdminMenu();
                            menu.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                JOptionPane.showMessageDialog(null, "Nu exista acest user sau ati introdus parola gresita!");
            }

        } catch (SQLException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE, null, ex);
    }   
    }//GEN-LAST:event_loginActionPerformed

    private void userNameTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_userNameTextActionPerformed
        // TODO add your handling code here:
          //Connection  conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/amicus?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root","");
            String sql = "select idUser,username,parola,tipUser from user where username = ? and parola = ?";
            PreparedStatement st = conn.prepareStatement(sql);

            st.setString(1, userNameText.getText());
            st.setString(2, passWordText.getText());
            userName = userNameText.getText();

            ResultSet rs = st.executeQuery();

            System.out.println("DONE");

            if (rs.next()) {
                String tip = rs.getString("tipUser");
                //System.out.println(tip);
                if (tip.equals("voluntarAmicus")) {
                    this.setVisible(false);

                    String SQL = "select idVoluntar from voluntaramicus amicus where idVoluntar = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        String sqlVol = "update user set enabled=1 where idUser = ?";
                        PreparedStatement stVol = conn.prepareStatement(sqlVol);
                        stVol.setInt(1, id);
                        stVol.execute();

                        JOptionPane.showMessageDialog(null, "Welcome voluntar!");
                        VoluntarMenu menu = new VoluntarMenu();
                        menu.setVisible(true);
                    }

                }

                if (tip.equals("sponsor")) {
                    this.setVisible(false);

                    String SQL = "select idSponsor from sponsor  where idSponsor = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        try {
                            String sqlSponsor = "update user set enabled=1 where idUser = ?";
                            PreparedStatement stSponsor = conn.prepareStatement(sqlSponsor);
                            stSponsor.setInt(1, id);
                            stSponsor.execute();

                            JOptionPane.showMessageDialog(null, "Welcome sponsor!");

                            SponsorMenu menu = new SponsorMenu();
                            menu.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                if (tip.equals("administrator")) {
                    this.setVisible(false);

                    String SQL = "select idAdmin from administrator  where idAdmin = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        try {
                            String sqlAdmin = "update user set enabled=1 where idUser = ?";
                            PreparedStatement stAdmin = conn.prepareStatement(sqlAdmin);
                            stAdmin.setInt(1, id);
                            stAdmin.execute();

                            JOptionPane.showMessageDialog(null, "Welcome admin!");

                            AdminMenu menu = new AdminMenu();
                            menu.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                JOptionPane.showMessageDialog(null, "Nu exista acest user sau ati introdus parola gresita!");
            }

        } catch (SQLException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE, null, ex);
    }   
    }//GEN-LAST:event_userNameTextActionPerformed

    private void passWordTextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passWordTextActionPerformed
        // TODO add your handling code here:
          //Connection  conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/amicus?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root","");
            String sql = "select idUser,username,parola,tipUser from user where username = ? and parola = ?";
            PreparedStatement st = conn.prepareStatement(sql);

            st.setString(1, userNameText.getText());
            st.setString(2, passWordText.getText());
            userName = userNameText.getText();

            ResultSet rs = st.executeQuery();

            System.out.println("DONE");

            if (rs.next()) {
                String tip = rs.getString("tipUser");
                //System.out.println(tip);
                if (tip.equals("voluntarAmicus")) {
                    this.setVisible(false);

                    String SQL = "select idVoluntar from voluntaramicus amicus where idVoluntar = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        String sqlVol = "update user set enabled=1 where idUser = ?";
                        PreparedStatement stVol = conn.prepareStatement(sqlVol);
                        stVol.setInt(1, id);
                        stVol.execute();

                        JOptionPane.showMessageDialog(null, "Welcome voluntar!");
                        VoluntarMenu menu = new VoluntarMenu();
                        menu.setVisible(true);
                    }

                }

                if (tip.equals("sponsor")) {
                    this.setVisible(false);

                    String SQL = "select idSponsor from sponsor  where idSponsor = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        try {
                            String sqlSponsor = "update user set enabled=1 where idUser = ?";
                            PreparedStatement stSponsor = conn.prepareStatement(sqlSponsor);
                            stSponsor.setInt(1, id);
                            stSponsor.execute();

                            JOptionPane.showMessageDialog(null, "Welcome sponsor!");

                            SponsorMenu menu = new SponsorMenu();
                            menu.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                }

                if (tip.equals("administrator")) {
                    this.setVisible(false);

                    String SQL = "select idAdmin from administrator  where idAdmin = ?";
                    PreparedStatement st2 = conn.prepareStatement(SQL);
                    int id = rs.getInt("idUser");

                    System.out.println(id);
                    st2.setInt(1, id);

                    ResultSet rs2 = st2.executeQuery();
                    //System.out.println(rs2.next());

                    //e voluntar
                    if (st2.execute()) {

                        try {
                            String sqlAdmin = "update user set enabled=1 where idUser = ?";
                            PreparedStatement stAdmin = conn.prepareStatement(sqlAdmin);
                            stAdmin.setInt(1, id);
                            stAdmin.execute();

                            JOptionPane.showMessageDialog(null, "Welcome admin!");

                            AdminMenu menu = new AdminMenu();
                            menu.setVisible(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

            } else {
                JOptionPane.showMessageDialog(null, "Nu exista acest user sau ati introdus parola gresita!");
            }

        } catch (SQLException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(LoginForm.class.getName()).log(Level.SEVERE, null, ex);
    }   
    }//GEN-LAST:event_passWordTextActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JButton login;
    private javax.swing.JPasswordField passWordText;
    private javax.swing.JTextField userNameText;
    // End of variables declaration//GEN-END:variables
}
