/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicus_v3;

import java.sql.Date;

/**
 *
 * @author Asus
 */
public class Boboc {
    private String nume;
    private String prenume;
    private Date dataNasterii;
    
    
    
    
    public Boboc(String nume,String prenume,Date dataNasterii)
    {
        this.nume = nume;
        this.prenume = prenume;
        this.dataNasterii = dataNasterii;
        
    }
    /**
     * @return the nume
     */
    public String getNume() {
        return nume;
    }

    /**
     * @param nume the nume to set
     */
    public void setNume(String nume) {
        this.nume = nume;
    }

    /**
     * @return the prenume
     */
    public String getPrenume() {
        return prenume;
    }

    /**
     * @param prenume the prenume to set
     */
    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    /**
     * @return the data
     */
    public Date getDataNasterii() {
        return dataNasterii;
    }

    /**
     * @param data the data to set
     */
    public void setDataNasterii(Date dataNasterii) {
        this.dataNasterii = dataNasterii;
    }
    
    
    
}
