/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicus_v3;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author Asus
 */
public class Eveniment {
    private int idEveniment;
    private int idLocatie;
    private int idDepartament;
    private int idComitet;
    private String numeEveniment;
    private Date dataInceputEveniment;
    private Date dataSfarsitEveniment;
    private Time oraInceput;
    private Time oraSfarsit;
    private String tipEveniment;
    public Eveniment(int idEveniment,int idLocatie,int idDepartament,int idComitet,String numeEveniment,Date dataInceputEveniment,Date dataSfarsitEveniment,Time oraInceput,Time oraSfarsit,String tipEveniment)
    {
        this.idEveniment = idEveniment;
        this.idLocatie = idLocatie;
        this.idComitet = idComitet;
        this.idDepartament = idDepartament;
        this.numeEveniment = numeEveniment;
        this.dataInceputEveniment = dataInceputEveniment;
        this.dataSfarsitEveniment = dataSfarsitEveniment;
        this.oraInceput = oraInceput;
        this.oraSfarsit = oraSfarsit;
        this.tipEveniment = tipEveniment;
        this.idDepartament = idDepartament;
    }


    
    
    public int getIdEveniment()
    {
        return idEveniment;
    }
    public int getIdLocatie()
    {
        return idLocatie;
    }
    
    public int getIdComitet()
    {
        return idComitet;
    }
    public int getIdDepartament()
    {
        return idDepartament;
    }
    public String getNumeEveniment()
    {
        return numeEveniment;
    }
    
    public Date getDataInceputEveniment()
    {
        return dataInceputEveniment;
    }
    
    public Date getDataSfarsitEveniment()
    {
        return dataSfarsitEveniment;
    }
    
    public Time getOraInceput()
    {
        return oraInceput;
    }
    
    public Time getOraSfarsit()
    {
        return oraSfarsit;
    }
    public String getTipEveniment()
    {
        return tipEveniment;
    }
}
