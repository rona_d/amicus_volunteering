/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicus_v3;

/**
 *
 * @author Asus
 */
public class Locatie {
    private int idLocatie;
    private String oras;
    private String strada;
    private int numar;
    private String zona;
    
    
    public Locatie(int idLocatie,String oras,String strada,int numar,String zona)
    {
        this.idLocatie = idLocatie;
        this.oras = oras;
        this.strada = strada;
        this.numar = numar;
        this.zona = zona;
        
        
    }

    /**
     * @return the idLocatie
     */
    public int getIdLocatie() {
        return idLocatie;
    }

    /**
     * @param idLocatie the idLocatie to set
     */
    public void setIdLocatie(int idLocatie) {
        this.idLocatie = idLocatie;
    }

    /**
     * @return the oras
     */
    public String getOras() {
        return oras;
    }

    /**
     * @param oras the oras to set
     */
    public void setOras(String oras) {
        this.oras = oras;
    }

    /**
     * @return the strada
     */
    public String getStrada() {
        return strada;
    }

    /**
     * @param strada the strada to set
     */
    public void setStrada(String strada) {
        this.strada = strada;
    }

    /**
     * @return the numar
     */
    public int getNumar() {
        return numar;
    }

    /**
     * @param numar the numar to set
     */
    public void setNumar(int numar) {
        this.numar = numar;
    }

    /**
     * @return the zona
     */
    public String getZona() {
        return zona;
    }

    /**
     * @param zona the zona to set
     */
    public void setZona(String zona) {
        this.zona = zona;
    }
    
    
}
