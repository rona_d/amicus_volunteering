/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicus_v3;

/**
 *
 * @author Asus
 */
public class Departament {
    private int idDepartament;
    private String numeDepartament;
    private int idDirector;
    private int idComitet;
    
    private int idBuget;
    
    
    
    public Departament(int idDepartament,String numeDepartament,int idDirector,int idComitet,int idBuget)
    {
        this.idDepartament = idDepartament;
        this.numeDepartament = numeDepartament;
        this.idDirector = idDirector;
        this.idComitet = idComitet;
        this.idBuget = idBuget;
    }
    /**
     * @return the idDepartament
     */
    public int getIdDepartament() {
        return idDepartament;
    }

    /**
     * @param idDepartament the idDepartament to set
     */
    public void setIdDepartament(int idDepartament) {
        this.idDepartament = idDepartament;
    }

    /**
     * @return the numeDepartament
     */
    public String getNumeDepartament() {
        return numeDepartament;
    }

    /**
     * @param numeDepartament the numeDepartament to set
     */
    public void setNumeDepartament(String numeDepartament) {
        this.numeDepartament = numeDepartament;
    }

    /**
     * @return the idDirector
     */
    public int getIdDirector() {
        return idDirector;
    }

    /**
     * @param idDirector the idDirector to set
     */
    public void setIdDirector(int idDirector) {
        this.idDirector = idDirector;
    }

    /**
     * @return the idComitet
     */
    public int getIdComitet() {
        return idComitet;
    }

    /**
     * @param idComitet the idComitet to set
     */
    public void setIdComitet(int idComitet) {
        this.idComitet = idComitet;
    }

    /**
     * @return the idBuget
     */
    public int getIdBuget() {
        return idBuget;
    }

    /**
     * @param idBuget the idBuget to set
     */
    public void setIdBuget(int idBuget) {
        this.idBuget = idBuget;
    }
    
    
    
    
}
