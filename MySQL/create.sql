-- MySQL Workbench Forward Engineering

#Abel 25.12.2018 - Am modificat la buget,finantare,factura

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema amicus
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema amicus
DROP DATABASE IF EXISTS amicus;

CREATE DATABASE IF NOT EXISTS `amicus` DEFAULT CHARACTER SET latin1 ;
USE `amicus` ;

-- -----------------------------------------------------
-- Table `amicus`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`user` (
  `idUser` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(20) NULL DEFAULT NULL,
  `email` VARCHAR(30) NULL DEFAULT NULL,
  `parola` VARCHAR(15) NULL DEFAULT NULL,
  `tipUser` ENUM('voluntarAmicus', 'sponsor', 'administrator') NULL DEFAULT NULL,
  `enabled` BIT(1) NULL DEFAULT NULL,
  PRIMARY KEY (`idUser`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`administrator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`administrator` (
  `idAdmin` INT(11) NOT NULL AUTO_INCREMENT,
  `nume` VARCHAR(45) NULL DEFAULT NULL,
  `prenume` VARCHAR(45) NULL DEFAULT NULL,
  `idUser` INT(11) NULL DEFAULT NULL,
  `dataNasterii` DATE NULL DEFAULT NULL,
  `numeFacultate` VARCHAR(40) NULL DEFAULT NULL,
  `specializareFacultate` VARCHAR(30) NULL DEFAULT NULL,
  `nrTelefon` INT(11) NULL DEFAULT NULL,
  `anIntrareFacultate` YEAR(4) NULL DEFAULT NULL,
  `anAbsolvireFacultate` YEAR(4) NULL DEFAULT NULL,
  `statusCor` ENUM('sopran', 'alto', 'tenor', 'bas', 'nu cant') NULL DEFAULT NULL,
  `functie` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`idAdmin`),
  INDEX `idUser` (`idUser` ASC) ,
  CONSTRAINT `administrator_ibfk_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `amicus`.`user` (`idUser`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`comitet`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`comitet` (
  `idComitet` INT(11) NOT NULL AUTO_INCREMENT,
  `dataInceput` DATE NULL DEFAULT NULL,
  `dataSfarsit` DATE NULL DEFAULT NULL,
  `scop` VARCHAR(100) NULL DEFAULT NULL,
  PRIMARY KEY (`idComitet`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`buget`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`buget` (
  `idBuget` INT(11) NOT NULL AUTO_INCREMENT,
  `sumaCont` INT(11) NULL DEFAULT NULL,
  `sumaDonatii` INT(11) NULL DEFAULT NULL,       
  `valoareaBonurilor` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idBuget`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`departament`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`departament` (
  `idDepartament` INT(11) NOT NULL AUTO_INCREMENT,
  `numeDepartament` VARCHAR(20) NULL DEFAULT NULL,
  `idDirector` INT(11) NOT NULL,
  `idComitet` INT(11) NULL DEFAULT NULL,
  `idBuget` INT NULL,
  PRIMARY KEY (`idDepartament`),
  INDEX `idDirector` (`idDirector` ASC) ,
  INDEX `idComitet` (`idComitet` ASC) ,
  INDEX `idDepartamentBuget_idx` (`idBuget` ASC) ,
  CONSTRAINT `departament_ibfk_1`
    FOREIGN KEY (`idDirector`)
    REFERENCES `amicus`.`administrator` (`idAdmin`)
    ON DELETE NO ACTION,
  CONSTRAINT `departament_ibfk_3`
    FOREIGN KEY (`idComitet`)
    REFERENCES `amicus`.`comitet` (`idComitet`)
    ON DELETE SET NULL,
  CONSTRAINT `idDepartamentBuget`
    FOREIGN KEY (`idBuget`)
    REFERENCES `amicus`.`buget` (`idBuget`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`locatie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`locatie` (
  `idLocatie` INT(11) NOT NULL AUTO_INCREMENT,
  `oras` VARCHAR(20) NULL DEFAULT NULL,
  `strada` VARCHAR(30) NULL DEFAULT NULL,
  `numar` SMALLINT(6) NULL DEFAULT NULL,
  `zona` VARCHAR(30) NULL DEFAULT NULL,
  PRIMARY KEY (`idLocatie`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`proiect`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`proiect` (
  `idProiect` INT(11) NOT NULL AUTO_INCREMENT,
  `idDepartament` INT(11) NOT NULL,
  `idLocatie` INT(11) NOT NULL,
  `numeProiect` VARCHAR(45) NULL DEFAULT NULL,
  `descriereProiect` VARCHAR(500) NULL DEFAULT NULL,
  `dataInceput` DATE NULL DEFAULT NULL,
  `dataFinal` DATE NULL DEFAULT NULL,
  `idComitet` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idProiect`),
  INDEX `idDepartament` (`idDepartament` ASC) ,
  INDEX `idLocatie` (`idLocatie` ASC),
  INDEX `idComitet` (`idComitet` ASC),
  CONSTRAINT `proiect_ibfk_1`
    FOREIGN KEY (`idDepartament`)
    REFERENCES `amicus`.`departament` (`idDepartament`)
    ON DELETE CASCADE,
  CONSTRAINT `proiect_ibfk_2`
    FOREIGN KEY (`idLocatie`)
    REFERENCES `amicus`.`locatie` (`idLocatie`)
    ON DELETE CASCADE,
  CONSTRAINT `proiect_ibfk_3`
    FOREIGN KEY (`idComitet`)
    REFERENCES `amicus`.`comitet` (`idComitet`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`eveniment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`eveniment` (
  `idEveniment` INT(11) NOT NULL AUTO_INCREMENT,
  `idLocatie` INT(11) NOT NULL,
  `idDepartament` INT(11) NOT NULL,
  `idComitet` INT(11) NULL DEFAULT NULL,
  `numeEveniment` VARCHAR(200) NULL DEFAULT NULL,
  `dataInceputEveniment` DATE NULL DEFAULT NULL,
  `dataSfarsitEveniment` DATE NULL DEFAULT NULL,
  `oraInceput` TIME NULL DEFAULT NULL,
  `oraSfarsit` TIME NULL DEFAULT NULL,
  `tipEveniment` ENUM('Intalnire Amicus', 'Bobociada', 'Absolvire', 'Serata', 'Revelion', 'Excursie in afara', 'Vizita') NULL DEFAULT NULL,
  PRIMARY KEY (`idEveniment`),
  INDEX `idDepartament` (`idDepartament` ASC),
  INDEX `idLocatie` (`idLocatie` ASC) ,
  INDEX `idComitet` (`idComitet` ASC) ,
  CONSTRAINT `eveniment_ibfk_1`
    FOREIGN KEY (`idDepartament`)
    REFERENCES `amicus`.`departament` (`idDepartament`)
    ON DELETE CASCADE,
  CONSTRAINT `eveniment_ibfk_2`
    FOREIGN KEY (`idLocatie`)
    REFERENCES `amicus`.`locatie` (`idLocatie`)
    ON DELETE CASCADE,
  CONSTRAINT `eveniment_ibfk_3`
    FOREIGN KEY (`idComitet`)
    REFERENCES `amicus`.`comitet` (`idComitet`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`activitate`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`activitate` (
  `idActivitate` INT(11) NOT NULL AUTO_INCREMENT,
  `idProiect` INT(11) NULL DEFAULT NULL,
  `idEveniment` INT(11) NULL DEFAULT NULL,
  `descriere` VARCHAR(200) NULL DEFAULT NULL,
  `tipActivitate` ENUM('Afara','Inauntru') NULL DEFAULT NULL,
  `titlu` VARCHAR(50) NULL DEFAULT NULL,
  PRIMARY KEY (`idActivitate`),
  INDEX `idProiect` (`idProiect` ASC)  ,
  INDEX `idEveniment` (`idEveniment` ASC)  ,
  CONSTRAINT `activitate_ibfk_1`
    FOREIGN KEY (`idProiect`)
    REFERENCES `amicus`.`proiect` (`idProiect`)
    ON DELETE CASCADE,
  CONSTRAINT `activitate_ibfk_2`
    FOREIGN KEY (`idEveniment`)
    REFERENCES `amicus`.`eveniment` (`idEveniment`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`sponsor`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`sponsor` (
  `idSponsor` INT(11) NOT NULL AUTO_INCREMENT,
  `nume` VARCHAR(45) NULL DEFAULT NULL,
  `prenume` VARCHAR(20) NULL DEFAULT NULL,
  `functie` VARCHAR(45) NULL DEFAULT NULL,
  `companie` VARCHAR(100) NULL DEFAULT NULL,
  `iBAN` VARCHAR(30) NULL DEFAULT NULL, 
  `idUser` INT(11) NOT NULL,
  PRIMARY KEY (`idSponsor`),
  INDEX `idUser` (`idUser` ASC)  ,
  CONSTRAINT `sponsor_ibfk_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `amicus`.`user` (`idUser`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`finantare`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`finantare` (
  `idFinantare` INT(11) NOT NULL AUTO_INCREMENT,  #plus AUTO_INCREMENT
  `idProiect` INT(11) NULL DEFAULT NULL,
  `idEveniment` INT(11) NULL DEFAULT NULL,
  `idSponsor` INT(11) NOT NULL,
  PRIMARY KEY (`idFinantare`),
  INDEX `idSponsor` (`idSponsor` ASC)  ,
  INDEX `idProiect` (`idProiect` ASC)  ,
  INDEX `idEveniment` (`idEveniment` ASC)  ,
  CONSTRAINT `finantare_ibfk_1`
    FOREIGN KEY (`idSponsor`)
    REFERENCES `amicus`.`sponsor` (`idSponsor`)
    ON DELETE CASCADE,
  CONSTRAINT `finantare_ibfk_2`
    FOREIGN KEY (`idProiect`)
    REFERENCES `amicus`.`proiect` (`idProiect`)
    ON DELETE SET NULL,
  CONSTRAINT `finantare_ibfk_3`
    FOREIGN KEY (`idEveniment`)
    REFERENCES `amicus`.`eveniment` (`idEveniment`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`factura`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`factura` (
  `idFactura` INT(11) NOT NULL AUTO_INCREMENT,
  `dataEmitere` DATE NULL DEFAULT NULL,
  `oraEmitere` TIME NULL ,
  `sumaBonuri` INT(11) NULL DEFAULT NULL, #plus
  `suma` INT(11) NULL DEFAULT NULL,
  `idFinantare` INT(11) NOT NULL,
  PRIMARY KEY (`idFactura`),
  INDEX `idSponsor` (`idFinantare` ASC)  ,
  CONSTRAINT `factura_ibfk_1`
    FOREIGN KEY (`idFinantare`)
    REFERENCES `amicus`.`finantare` (`idFinantare`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`voluntaramicus`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`voluntaramicus` (
  `idVoluntar` INT(11) NOT NULL AUTO_INCREMENT,
  `nume` VARCHAR(20) NULL DEFAULT NULL,
  `prenume` VARCHAR(20) NULL DEFAULT NULL,
  `dataNasterii` DATE NULL DEFAULT NULL,
  `nrTelefon` INT(11) NULL DEFAULT NULL,
  `numeFacultate` VARCHAR(140) NULL DEFAULT NULL,
  `specializareFacultate` VARCHAR(130) NULL DEFAULT NULL,
  `anIntrareFacultate` YEAR(4) NULL DEFAULT NULL,
  `anAbsolvireFacultate` YEAR(4) NULL DEFAULT NULL,
  `hobbyuri` VARCHAR(220) NULL,
  `statusBotezat` BIT(1) NULL DEFAULT NULL,
  `statusCor` ENUM('sopran', 'alto', 'bas', 'tenor', 'nu') NULL DEFAULT NULL,
  `idUser` INT(11) NOT NULL,
  PRIMARY KEY (`idVoluntar`),
  INDEX `idUser` (`idUser` ASC)  ,
  CONSTRAINT `voluntaramicus_ibfk_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `amicus`.`user` (`idUser`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`implicare`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`implicare` (
  `idImplicare` INT(11) NOT NULL AUTO_INCREMENT,
  `idComitet` INT(11) NOT NULL,
  `idVoluntar` INT(11) NULL DEFAULT NULL,
  `idAdmin` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`idImplicare`),
  INDEX `idComitet` (`idComitet` ASC)  ,
  INDEX `idVoluntar` (`idVoluntar` ASC)  ,
  INDEX `idAdmin` (`idAdmin` ASC)  ,
  CONSTRAINT `implicare_ibfk_1`
    FOREIGN KEY (`idComitet`)
    REFERENCES `amicus`.`comitet` (`idComitet`)
    ON DELETE CASCADE,
  CONSTRAINT `implicare_ibfk_2`
    FOREIGN KEY (`idVoluntar`)
    REFERENCES `amicus`.`voluntaramicus` (`idVoluntar`)
    ON DELETE SET NULL,
  CONSTRAINT `implicare_ibfk_3`
    FOREIGN KEY (`idAdmin`)
    REFERENCES `amicus`.`administrator` (`idAdmin`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`invitat`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`invitat` (
  `idInvitat` INT(11) NOT NULL AUTO_INCREMENT,
  `idEveniment` INT(11) NULL DEFAULT NULL,
  `idProiect` INT(11) NULL DEFAULT NULL,
  `nume` VARCHAR(20) NULL DEFAULT NULL,
  `prenume` VARCHAR(20) NULL DEFAULT NULL,
  `oras` VARCHAR(15) NULL DEFAULT NULL,
  `job` VARCHAR(45) NULL DEFAULT NULL,
  PRIMARY KEY (`idInvitat`),
  INDEX `idEveniment` (`idEveniment` ASC)  ,
  INDEX `idProiect` (`idProiect` ASC)  ,
  CONSTRAINT `invitat_ibfk_1`
    FOREIGN KEY (`idEveniment`)
    REFERENCES `amicus`.`eveniment` (`idEveniment`)
    ON DELETE SET NULL,
  CONSTRAINT `invitat_ibfk_2`
    FOREIGN KEY (`idProiect`)
    REFERENCES `amicus`.`proiect` (`idProiect`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1
COMMENT = '    ';


-- -----------------------------------------------------
-- Table `amicus`.`spatiudepozitare`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`spatiudepozitare` (
  `idspatiuDepozitare` ENUM('Debara', 'Garaj') NOT NULL,
  `pretChirie` VARCHAR(10) NULL DEFAULT NULL,
  `idLocatie` INT(11) NOT NULL,
  PRIMARY KEY (`idspatiuDepozitare`),
  INDEX `idLocatie` (`idLocatie` ASC)  ,
  CONSTRAINT `spatiudepozitare_ibfk_1`
    FOREIGN KEY (`idLocatie`)
    REFERENCES `amicus`.`locatie` (`idLocatie`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`obiect`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`obiect` (
  `numeObiect` VARCHAR(45) NOT NULL,
  `cantitateObiect` VARCHAR(30),
  `pretObiect` MEDIUMINT(9) NULL DEFAULT NULL,
  `idSpatiuDepozitare` ENUM('Debara', 'Garaj') NOT NULL,
  PRIMARY KEY (`numeObiect`),
  INDEX `idSpatiuDepozitare` (`idSpatiuDepozitare` ASC)  ,
  CONSTRAINT `obiect_ibfk_1`
    FOREIGN KEY (`idSpatiuDepozitare`)
    REFERENCES `amicus`.`spatiudepozitare` (`idspatiuDepozitare`)
    ON DELETE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`participant`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`participant` (
  `idParticipant` INT(11) NOT NULL AUTO_INCREMENT,
  `idUser` INT(11) NOT NULL,
  `idEveniment` INT(11) NULL DEFAULT NULL,
  `idProiect` INT(11) NULL DEFAULT NULL,
  `tipParticipant` ENUM('voluntar', 'boboc', 'mentor', 'absolvent', 'guest') NULL DEFAULT NULL,
  PRIMARY KEY (`idParticipant`),
  INDEX `idUser` (`idUser` ASC)  ,
  INDEX `idEveniment` (`idEveniment` ASC)  ,
  INDEX `idProiect` (`idProiect` ASC)  ,
  CONSTRAINT `participant_ibfk_1`
    FOREIGN KEY (`idUser`)
    REFERENCES `amicus`.`user` (`idUser`)
    ON DELETE CASCADE,
  CONSTRAINT `participant_ibfk_2`
    FOREIGN KEY (`idEveniment`)
    REFERENCES `amicus`.`eveniment` (`idEveniment`),
  CONSTRAINT `participant_ibfk_3`
    FOREIGN KEY (`idProiect`)
    REFERENCES `amicus`.`proiect` (`idProiect`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`ianizatie`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`profilorganizatie` (
  `nume` VARCHAR(50) NOT NULL,
 -- `imagineLogo` MEDIUMBLOB NULL DEFAULT NULL,
  `dataInfiintare` DATE NULL DEFAULT NULL,
  `descriere` TEXT NULL DEFAULT NULL,
  `idLocatie` INT(11) NULL DEFAULT NULL,
  `presedinte` VARCHAR(45) NULL DEFAULT NULL,
  `nrTelefon` VARCHAR(11) NULL DEFAULT NULL,
  `idComitet` INT(11) NULL DEFAULT NULL,
  `CIF` INT(11) NULL DEFAULT NULL,
  `iBAN` VARCHAR(30) NULL DEFAULT NULL,
  `idBuget` INT NULL,
  PRIMARY KEY (`nume`),
  INDEX `idComitet` (`idComitet` ASC)  ,
  INDEX `idLocatie` (`idLocatie` ASC)  ,
  INDEX `idProfilBuget_idx` (`idBuget` ASC)  ,
  CONSTRAINT `profilorganizatie_ibfk_1`
    FOREIGN KEY (`idComitet`)
    REFERENCES `amicus`.`comitet` (`idComitet`)
    ON DELETE SET NULL,
  CONSTRAINT `profilorganizatie_ibfk_3`
    FOREIGN KEY (`idLocatie`)
    REFERENCES `amicus`.`locatie` (`idLocatie`)
    ON DELETE SET NULL,
  CONSTRAINT `idProfilBuget`
    FOREIGN KEY (`idBuget`)
    REFERENCES `amicus`.`buget` (`idBuget`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `amicus`.`stand`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `amicus`.`stand` (
  `idstand` INT(10) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT,
  `numeStand` VARCHAR(20) NULL DEFAULT NULL,
  `descriereActivitateStand` VARCHAR(80) NULL DEFAULT NULL,
  `responsabiliStand` INT(11) NULL DEFAULT NULL,
  `nrMeseNecesare` TINYINT(4) NULL DEFAULT NULL,
  `nrScauneNecesare` TINYINT(4) NULL DEFAULT NULL,
  `areStampila` BIT(1) NULL DEFAULT NULL,
  `materialeNecesare` VARCHAR(200) NULL DEFAULT NULL,
  `bugetEstimat` SMALLINT(6) NULL DEFAULT NULL,
  `nrVoluntariNecesari` TINYINT(4) NULL DEFAULT NULL,
  `idProiect` INT(11) NOT NULL,
  PRIMARY KEY (`idstand`),
  INDEX `idProiect` (`idProiect` ASC)  ,
  INDEX `responsabiliStand` (`responsabiliStand` ASC)  ,
  CONSTRAINT `stand_ibfk_1`
    FOREIGN KEY (`idProiect`)
    REFERENCES `amicus`.`proiect` (`idProiect`)
    ON DELETE CASCADE,
  CONSTRAINT `stand_ibfk_2`
    FOREIGN KEY (`responsabiliStand`)
    REFERENCES `amicus`.`user` (`idUser`)
    ON DELETE SET NULL)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

drop table if exists userBackUp;
create table userBackUp ( idUserB int(11), usernameB varchar(20), emailB varchar(30), parolaB varchar(15),
	tipUserB enum('voluntarAmicus','sponsor','administrator'), timestamp time, datestamp date );


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;




