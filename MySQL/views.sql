use amicus;

drop view if exists proiecte_view2;
CREATE VIEW proiecte_view2 AS
select p.numeProiect As Denumire,p.descriereProiect as Descriere,
p.dataInceput as Data_de_Inceput, p.dataFinal as Data_de_Finalizare, l.oras as Oras, l.strada As Strada, l.numar as Numar
from proiect p inner join locatie l 
using (idLocatie) ;


drop view if exists proiecte_view;
CREATE VIEW proiecte_view AS
select p.numeProiect As Denumire,p.descriereProiect as Descriere,
p.dataInceput as 'Data de Inceput',p.dataFinal as 'Data de Finalizare', l.oras as Oras, l.strada As Strada, l.numar as Numar
from proiect p inner join locatie l 
using (idLocatie) ;

drop view if exists evenimente_view;
CREATE VIEW evenimente_view AS
select e.numeeveniment as Denumire, e.dataInceputEveniment as 'Data',
CONCAT( i.nume,' ',i.prenume) As 'Invitat Special', l.strada As Strada, l.numar as Nr , l.oras as Oras, l.zona as 'Zona'
from (eveniment e left join invitat i using(idEveniment)) left join locatie l using (idLocatie);


drop view if exists departamente_view;
CREATE VIEW departamente_view AS 
select d.numedepartament as 'Departament', concat(v.nume,' ',v.prenume) as 'Director'  
from departament d left join administrator v on (d.idDirector=v.idAdmin);


drop view if exists tranzactii_view;
CREATE VIEW tranzactii_view AS
select f.idFinantare As 'ID Finantare', s.iBAN as 'IBAN Sponsor', p.iBAN as 'IBAN Amicus' ,  fac.suma + fac.sumabonuri as 'Total'
from sponsor s inner join profilorganizatie p inner join finantare f using (idSponsor) inner join factura fac using (idFinantare);


drop view if exists proiecte_view_noi;
CREATE VIEW proiecte_view_noi AS
select p.numeProiect As Nume, p.descriereProiect as Descriere, 
p.dataInceput as 'Data Inceput', p.dataFinal as 'Data Sfarsit', 
CONCAT(l.oras, '   ', l.strada,'  ', l.numar, '  ', l.zona, ' ') as Locatie
from proiect p inner join locatie l 
using (idLocatie) 
where dataInceput > date(now());


drop view if exists evenimente_view_noi;
CREATE VIEW evenimente_view_noi AS
select e.numeeveniment as Nume, e.dataInceputEveniment as 'Data',
CONCAT( i.nume,' ',i.prenume) As 'Invitat Special', l.strada As Strada, l.numar as Nr , l.oras as Oras, l.zona as 'Zona'
from (eveniment e left join invitat i using(idEveniment)) left join locatie l using (idLocatie)
where dataInceputEveniment > date(now()) AND e.idEveniment NOT IN (SELECT idEveniment from participant);


drop view if exists proiecte_view_istoric;
CREATE VIEW proiecte_view_istoric AS
select p.numeProiect As Nume, p.descriereProiect as Descriere, 
p.dataInceput as Data_Inceput, p.dataFinal as 'Data Sfarsit',l.oras as Oras, l.strada As Strada, l.numar as Numar, l.zona as Zona 
from proiect p inner join locatie l
using (idLocatie) 
where dataInceput < date(now());


drop view if exists evenimente_view_istoric;
CREATE VIEW evenimente_view_istoric AS
select e.numeeveniment as Nume, e.dataInceputEveniment as 'Data',
CONCAT( i.nume,' ',i.prenume) As Invitat_Special, l.strada As Strada, l.numar as Nr , l.oras as Oras, l.zona as 'Zona'
from (eveniment e left join invitat i using(idEveniment)) left join locatie l using (idLocatie)
where dataInceputEveniment < date(now());


# 2 viewuri cu fetch in acelasi tabel
drop view if exists proiecte_participare_view;
CREATE VIEW proiecte_participare_view AS
select p.numeProiect As Nume,
l.oras as Oras, l.strada As Strada, l.numar as Numar, l.zona as Zona 
from ((proiect p  inner join locatie l ON p.idLocatie = l.idLocatie) inner join participant particip ON p.idProiect = particip.idProiect);
 
drop view if exists evenimente_participare_view;
CREATE VIEW evenimente_participare_view AS
select e.numeEveniment As Nume,
l.oras as Oras, l.strada As Strada, l.numar as Numar, l.zona as Zona 
from ((eveniment e  inner join locatie l ON e.idLocatie = l.idLocatie) inner join participant particip ON e.idEveniment = particip.idEveniment);
 

CREATE  OR REPLACE VIEW `online_users` AS
select idUser,username,email,tipUser from user where enabled=1;


create or replace view `voluntar_search` As
select idUser as IDUser,idVoluntar as IDVoluntar,nume as Nume,prenume as Prenume,dataNasterii As DataNasterii,
nrTelefon as NrTelefon,numeFacultate as Facultate, specializareFacultate as Specializare
 from voluntaramicus;

create or replace view `sponsor_search` As
select idUser as IDUser,idSponsor as IDSponsor,nume As Nume, prenume as Prenume,functie as Functie,companie as Companie,
iBAN as IBAN from sponsor;

create or replace view `comitet_search` As
select idComitet as IDComitet, dataInceput as Data_Inceput, dataSfarsit as Data_Sfarsit, scop As Scop from comitet;


CREATE OR REPLACE VIEW `obiect_garaj` AS
select * from obiect inner join spatiudepozitare using (idspatiudepozitare) where idspatiudepozitare='Garaj';


CREATE OR REPLACE VIEW `obiect_debara` AS
select * from obiect inner join spatiudepozitare using (idspatiudepozitare) where idspatiudepozitare='Debara';
/*
drop view if exists proiecte_view_noi;
CREATE VIEW proiecte_view_noi AS
select p.numeProiect As Nume, p.descriereProiect as Descriere, 
p.dataInceput as 'Data Inceput', p.dataFinal as 'Data Sfarsit', 
CONCAT(l.oras, '   ', l.strada,'  ', l.numar, '  ', l.zona, ' ') as Locatie
from proiect p inner join locatie l 
using (idLocatie) 
where dataInceput > date(now());


drop view if exists evenimente_view_noi;
CREATE VIEW evenimente_view_noi AS
select e.numeeveniment as Nume, e.dataInceputEveniment as 'Data',
CONCAT( i.nume,' ',i.prenume) As 'Invitat Special', l.strada As Strada, l.numar as Nr , l.oras as Oras, l.zona as 'Zona'
from (eveniment e left join invitat i using(idEveniment)) left join locatie l using (idLocatie)
where dataInceputEveniment > date(now()) AND e.idEveniment NOT IN (SELECT idEveniment from participant);


drop view if exists proiecte_view_istoric;
CREATE VIEW proiecte_view_istoric AS
select p.numeProiect As Nume, p.descriereProiect as Descriere, 
p.dataInceput as Data_Inceput, p.dataFinal as 'Data Sfarsit',l.oras as Oras, l.strada As Strada, l.numar as Numar, l.zona as Zona 
from proiect p inner join locatie l
using (idLocatie) 
where dataInceput < date(now());


drop view if exists evenimente_view_istoric;
CREATE VIEW evenimente_view_istoric AS
select e.numeeveniment as Nume, e.dataInceputEveniment as 'Data',
CONCAT( i.nume,' ',i.prenume) As Invitat_Special, l.strada As Strada, l.numar as Nr , l.oras as Oras, l.zona as 'Zona'
from (eveniment e left join invitat i using(idEveniment)) left join locatie l using (idLocatie)
where dataInceputEveniment < date(now());
*/

# 2 viewuri cu fetch in acelasi tabel
drop view if exists proiecte_participare_view;
CREATE VIEW proiecte_participare_view AS
select p.numeProiect As Nume,
l.oras as Oras, l.strada As Strada, l.numar as Numar, l.zona as Zona 
from ((proiect p  inner join locatie l ON p.idLocatie = l.idLocatie) inner join participant particip ON p.idProiect = particip.idProiect);
 
drop view if exists evenimente_participare_view;
CREATE VIEW evenimente_participare_view AS
select e.numeEveniment As Nume,
l.oras as Oras, l.strada As Strada, l.numar as Numar, l.zona as Zona 
from ((eveniment e  inner join locatie l ON e.idLocatie = l.idLocatie) inner join participant particip ON e.idEveniment = particip.idEveniment);
 
