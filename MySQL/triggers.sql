use amicus;

drop trigger if exists make_backup_user;
delimiter //
create trigger make_backup_user
after  update
	on user for each row
    
    begin
    
    update userBackUp set usernameB =old.username where idUserb=old.idUser;
	update userBackUp set emailB =old.email where idUserb=old.idUser;
	update userBackUp set parolaB =old.parola where idUserb=old.idUser;
    update userBackUp set tipUserB =old.tipUser where idUserb=old.idUser;
    
    update  userBackUp set timestamp=current_time() where idUserb=old.idUser;
	update  userBackUp set datestamp=now() where idUserb=old.idUser;
    
    
	end//
delimiter ;

drop trigger if exists insert_backup_user;
delimiter //
create trigger insert_backup_user
after  insert
	on user for each row
    
    begin
    
    insert into userBackUp values (new.idUser, new.username, new.email,new.parola,new.tipUser,  current_time(),now() );
			
          
	end//
delimiter ;