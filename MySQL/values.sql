use amicus;

#####################################
--               USER                --
#####################################

insert into user values(4,'emmaasanache','emmaasanache@gmail.com','1234','voluntarAmicus',0);
insert into user values(5,'dia.brenda','dia.brenda@gmail.com','1234','voluntarAmicus',0);
insert into user values(6,'paul.dragota','paul.dragota@yahoo.com','1234','voluntarAmicus',0);
insert into user values(7,'andreea.boraa','andreea.boraa@gmail.com','1234','voluntarAmicus',0);
insert into user values(8,'mariusserban','twistlemonade@gmail.com','1234','voluntarAmicus',0);
insert into user values(9,'sabina.dirtu8','sabina.dirtu8@yahoo.com','1234','voluntarAmicus',0);
insert into user values(10,'damianlapuste74','damianlapuste74@gmail.com','1234','voluntarAmicus',0);
insert into user values(11,'ionut.iliesi','ionut.iliesi@gmail.com','1234','voluntarAmicus',0);
insert into user values(12,'martasasu','martasasu@yahoo.com','1234','voluntarAmicus',0);
insert into user values(13,'denisa.raluca112','denisa.raluca112@gmail.com','1234','voluntarAmicus',0);
insert into user values(14,'ligia.oana16','ligia.oana16@gmail.com','1234','voluntarAmicus',0);
insert into user values(15,'olari_rares','olari_rares@yahoo.com','1234','voluntarAmicus',0);
insert into user values(16,'robert.stef116','robert.stef116@gmail.com','1234','voluntarAmicus',0);
insert into user values(17,'scafarudina','scafarudina@gmail.com','1234','voluntarAmicus',0);
insert into user values(18,'dianarus_25','dianarus_25@yahoo.com','1234','voluntarAmicus',0);
insert into user values(19,'3duard26','3duard26@gmail.com','1234','voluntarAmicus',0);
insert into user values(20,'tudor_rotar99','tudor_rotar99@yahoo.com','1234','voluntarAmicus',0);
insert into user values(21,'madda_grs','madda_grs@yahoo.com','1234','voluntarAmicus',0);
insert into user values(22,'cristea.alfred','cristea.alfred@gmail.com','1234','voluntarAmicus',0);
insert into user values(23,'barbuioanalexandru','barbuioanalexandru@gmail.com','1234','voluntarAmicus',0);
insert into user values(24,'dtbianca77','dtbianca77@gmail.com','1234','voluntarAmicus',0);
insert into user values(25,'rebeca.struta','rebeca.struta@gmail.com','1234','voluntarAmicus',0);
insert into user values(26,'anca.bn01','anca.bn01@gmail.com','1234','voluntarAmicus',0);
insert into user values(27,'emy.oncea','emy.oncea@gmail.com','1234','voluntarAmicus',0);
insert into user values(28,'amalia.stavar','amalia.stavar@yahoo.com','1234','voluntarAmicus',0);
insert into user values(29,'eddiestanescu','eddiestanescu@gmail.com','1234','voluntarAmicus',0);
insert into user values(30,'sindy_luana','sindy_luana@yahoo.com','1234','voluntarAmicus',0);
insert into user values(31,'davidtivig','davidtivig@ymail.com','1234','voluntarAmicus',0);
insert into user values(32,'melania_betty_95','melania_betty_95@yahoo.com','1234','voluntarAmicus',0);
insert into user values(33,'iarinadanaila','iarinadanaila@yahoo.com','1234','voluntarAmicus',0);
insert into user values(34,'zsoldosboti','zsoldosboti@yahoo.com','1234','voluntarAmicus',0);
insert into user values(35,'piturleadenis','piturleadenis@gmail.com','1234','voluntarAmicus',0);
insert into user values(36,'robert.pacurariu','robert.pacurariu@yahoo.com','1234','voluntarAmicus',0);
insert into user values(37,'stefan.cosman','stefan.cosman@yahoo.com','1234','voluntarAmicus',0);
insert into user values(38,'andreeaghiurcuta','andreeaghiurcuta@gmail.com','1234','voluntarAmicus',0);
insert into user values(39,'danantonia14','danantonia14@yahoo.com','1234','voluntarAmicus',0);
insert into user values(40,'beniaminardelean','beniaminardelean@yahoo.com','1234','voluntarAmicus',0);
insert into user values(41,'micalmiha','micalmiha@ymail.con','1234','voluntarAmicus',0);
insert into user values(42,'morariuBrigita','vmbihp@gmail.com','1234','voluntarAmicus',0);
insert into user values(43,'horicoman','horicoman@yahoo.com','1234','voluntarAmicus',0);
insert into user values(44,'marianapopc','marianapopc@gmail.com','1234','voluntarAmicus',0);
insert into user values(45,'petretchiradu15','petretchiradu15@yahoo.com','1234','voluntarAmicus',0);
insert into user values(46,'mosoiuandra','mosoiuandra@gmail.com','1234','voluntarAmicus',0);
insert into user values(47,'sergiuturturica','sergiuturturica@yahoo.com','1234','voluntarAmicus',0);
insert into user values(48,'clambaSebastian','zzz_seby_zzz@yahoo.com','1234','voluntarAmicus',0);
insert into user values(49,'cin.na_in','cin.na_in@yahoo.com','1234','voluntarAmicus',0);
insert into user values(50,'damarisandra67','damarisandra67@gmail.com','1234','voluntarAmicus',0);
insert into user values(51,'ralucapodosu','ralucapodosu@outlook.com','1234','voluntarAmicus',0);
insert into user values(52,'adrianpasca13','adrianpasca13@gmail.com','1234','voluntarAmicus',0);
insert into user values(53,'narci.gui','narci.gui@gmail.com','1234','voluntarAmicus',0);
insert into user values(57,'bibjudOG','bibjudOG@gmail.com','1234','sponsor',0);
insert into user values(58,'muznatist','muznatist@gmail.com','1234','sponsor',0);
insert into user values(59,'sanovita','sanovita@gmail.com','1234','sponsor',0);
insert into user values(60,'MDdentalj','MDdentalj@gmail.com','1234','sponsor',0);
insert into user values(61,'NYDENTAL','NYDENTAL@gmail.com','1234','sponsor',0);
insert into user values(62,'learn_w_music','learn_w_music@gmail.com','1234','sponsor',0);
insert into user values(63,'kauflandro','kaufland@gmail.com','1234','sponsor',0);
insert into user values(64,'diverta','diverta@gmail.com','1234','sponsor',0);
insert into user values(65,'lehaci.brendadiana','lehaci.brendadiana@gmail.com','1234','voluntarAmicus',0);


#####################################
--              SPONSOR                --
#####################################

INSERT INTO sponsor VALUES
#idSponsor nume prenume functie companie idUser
(1,'Varga','Sebastian','secretar','Biblioteca Judeteana "Ocatvian Goga','RO69BTRL01301205N60457XXr',57),
(2,'Sepsi','Renata','secretar','Muzeul National de Istorie','RO69BTRL01301205N60457XXt',58),
(3,'Mihai','Marcian','ceo','SanoVita','RO69BTRL01301205N60457XXu',59),
(4,'Pop','Florin','secretar','MD Dental Junior','RO69BTRL01301205N60457XXv',60),
(5,'Nemes','Andreea','secretar','New York Dental','RO69BTRL01301205N60457XXw',61),
(6,'Sincai','Mihai','resurse umane','Learn with Music','RO69BTRL01301205N60457XXx',62),
(7,'Fjodor','Erno','secretar','Kaufland','RO69BTRL01301205N60457XXy',63),
(8,'Mihalcea','Robert','secretar','Diverta','RO69BTRL01301205N60457XXz',64);




#####################################
--        ADMINISTRATOR            --
#####################################

INSERT INTO administrator(idAdmin,nume,prenume) VALUES (1001,'Caraveteanu','Madalina');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1002,'Stefan','Raluca');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1003,'Cristea','Brita');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1004,'Dragu','Simon');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1005,'Rotaru','Anamaria');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1006,'Parascinet','Doru');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1007,'Ciutan','Iosana');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1008,'Farcas','Cristian');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1009,'Negru','Sindy');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1010,'Tivig','David');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1011,'Gui','Narcisa');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1012,'Muscalu','Alexandra');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1013,'Hura','Abel');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1014,'Prodan','Alexandru');
INSERT INTO administrator(idAdmin,nume,prenume) VALUES(1015,'Dumitrescu','Rona');



#####################################
--        VOLUNTARAMICUS          --
#####################################

INSERT INTO voluntarAmicus VALUES 
(1,'Bora','Andreea','1998-01-18',0727742925,'Business','Administrarea Afacerilor Eng',2017,2020,null,0,null,7),
(2,'Asanache','Emma','2002-02-07',0754580863,'LTA','profil liceu',2015,2019,'Sport(volei), muzica, calatorii',1,'alto',4),
(3,'Lehaci','Brenda-Diana','1999-03-06',0720086592,'Facultatea de Stiinte Politice, Administrative si ale Comunicarii','Public Health',2018,2021,null,0,null,65),
(4,'Serban','Marius','1992-12-06',0753460475,null,null,null,null,'Foto, calatorii, tech',1,null,8),
(5,'Dirtu','Sabina','1997-11-12',0747624242,'Facultatea de Psihologie si Stiinte ale Educatiei','Psihologie',2017,2020,'social, recreativ, cultural',1,null,9),
(6,'Lapuste','Demian',null,0757860615,'angajat',null,null,null,null,0,null,10),
(7,'Iliesi','Ionut','1996-09-19',0754931044,'UTCN','ETTI',2016,2020,'Timp petrecut cu prietenii, seriale, somnul(asta daca se pune)',1,null,11),
(8,'Sasu','Marta-Ramona','2000-01-16',0756461829, 'UBB','Drept',2018,2022,'Muzica,plimbarile,aventurile,lucrurile noi si provocatoare.',0,null,12),
(9,'Gaga','Denisa','1992-12-01',null,'UMF','Asistent Medical',null,null,null,1,null,13),
(10,'Circo','Ligia','1997-12-16',0784181066,'Facultatea de Geografie','Geografia Turismului',2016,2019,'Calatoriile',0,null,14),
(11,'Olari','Rares-Adrian', '1999-01-13',0748950593,'UMF','BFK',2018,2021,'bascket , drumetiile , muzica ',1,null,15),
(12,'Stefanica','Robert','1998-01-17',null,'UBB','Informatica',2017,2020,null,1,'tenor',16),
(13,'Scafaru','Dina', '1996-02-23' ,0751499808,'FSEGA','Strategii si politici de Marketing',2018,2020,'Muzica, cartile, somnul, prietenii,',1, 'sopran',17),
(14,'Rus','Diana','1999-05-25',0745285338,'Facultatea de Psihologie','Facultatea de Jurnalism',2018,2021,'Cant, ma plimb, joc tenis, citesc, desenez (nu prea frumos, dar imi place sa o fac)',1,null,18),
(15,'Gheroghita','Eduard','1999-11-26',0744894646 ,'AMGD, Facultatea de Interpretare','Pian',2018,2022,'Drumetiile, sportul, cititul, muzica',0,null,19),
(16,'Rotar','Calin-Tudor','1999-03-09',0748638491,'UTCN','Inginerie si menegement, inginerie economica industriala',2018,2021,'social',0,null,20),
(17,'Grigoras','Diana','1998-01-03',0787529871,'Facultatea de Geografie','Planificare Teritoriala',2017,2020,'Calatorii drumetii nutritie&sport  cantat in dus si arta cat incape ',1,'sopran',21),
(18,'Cristea','Alfred','1999-11-04',0754466271,'UTCN','Inginerie industriala TCM',2018,2022,'Ciclism, ping-Pong',0,null,22),
(19,'Barbu','Alexandru','1988-11-17',0758627750,'Data Scientist','ML & AI',2009,2017,'Citit, Filme, Muzica, Calatorit',0,null,23),
(20,'Draghici','Bianca','1998-04-03',0757676588,'UBB-Master','Psihologie',2018,2019,'Cititul, muntele, desenatul, sa cunosc oameni faini :p ',1,null,24),
(21,'Struta','Rebeca','1998-01-08',0723023500,'FSEGA','Economie si afaceri internationale',2017,2020,'Excursiile, sporturile extreme, seriale, romane psihologice, muzica, curatenia ',0,null,25),
(22,'Constantin','Ancuta','1996-12-01',0736320254,'UMF','Asistenta Medicala',2016,2020,'Citit, vizitat prieteni si vizionat filme ',1,null,26),
(23,'Oncea','Emanuel','1997-12-01',0761392525,'Facultatea de Mecanica','Mecatronica',2018,2022,'Social, Recreatic, Cultural',1,null,27),
(24,'Stavar','Amalia','1998-05-02',0751767231,'UMF','Asistenta Medicala Generala ',2017,2021,'Proiecte',1,null,28),
(25,'Bartha','Melania','1995-11-08',0746727915,'Facultatea de Matematica si informatica','Sisteme distribuite in internet, nivel master',2018,2020,'Volei, citit, calatorit, plimbat',1,null,32),
(26,'Iarina','Daniela','1997-06-06',0758535577,'UAD - Istoria si Teoria Artelor','AMGD - Pian',2016,2019,'Cultural',1,null,33),
(27,'Zoldos','Botond-Mihaly','1996-12-01',0746970935,', Facultatea Constructii de Masini','Specializarea Roboti Industriali',2015,2020,'Fotografie, mers pe bicicleta, snowboard',1,null,34),
(28,'Piturlea','Denis','1998-06-06',0770595208,' FSEGA','Economics and International Business',2018,2021,'Music, group activities, movie night, photography, volleyball, swimming, chess, and more recently, cooking :))',1,null,35),
(29,'Pacurariu','Robert','1999-12-15',0743151299,'Facultatea de Litere','Limbi Moderne Aplicate (engleza-germana)',2018,2021,'mult sport de orice fel, moto, foto-video, munti',0,null,36),
(30,'Stefan','Cosman','1995-11-21',0723445527,'Facultatea de business','Administrare afacerilor ',2018,2021,'munte',1,null,37),
(31,'Ghiurcuta','Andreea','1998-05-27',0757573554,'Matematica si Informatica','Matematica-Informatica engleza',2017,2020,'Cititul, cantarul la chitara, desenatul, ascult carai audio(engleza), gatitul',0,null,38),
(32,'Antonia','Dan','1998-06-27',0786482410,'Facultatea de Psihologie si Stiinte ale Educatiei','Psihologie',2017,2020,'Scrisul,cititul,uitatul la filme,calatoritul(drumetii),cantatul vocal sau la pian,arta.',0,null,39),
(33,'Ardelean','Beniamin','1996-09-05',0755291371,'Facultatea de Litere','Spaniola-Germana',null,null,'Muzica,Sport',1,null,40),
(34,'Pisuc','Mical','1998-06-30',0755361357,'UMF','Farmacie',2017,2022,'Social si Cultural',1,null,41),
(35,'Coman','Horatiu','1998-03-09',0768849014,'Facultatea de Business','Administrarea Afacerilor ',2017,2020,'Social si Proiecte',0,null,43),
(36,'Pop','Mariana','2000-04-05',0732181358,' Facultatea de stiinte politice administrative si ale comunictrii','Administratie publica',2018,2021,'Social,Recreativ',0,null,44),
(37,'Petretchi','Vasile-Radu','1996-02-19',0745247804,'UTCN','ETTI',2014,2020,'Studying , Chess, Fotbal, Street workout...',1,null,45),
(38,'Mosoiu','Andra-Monica','1999-06-07',0746885891,'Constructii','Inginerie Economica',2018,2022,null,1,null,46),
(39,'Clamba','Sebastian','1999-07-18',0741774041,'Academia de Muzica','Flaut',2018,2022,'Recreativ',1,null,48),
(40,'Tudorie','Catalina','1999-04-24',0769274985,'Litere','Coreeana - Ebraica',2018,2021,'Muzica, fotografie, sport, carti, desen, afaceri',1,null,49),
(41,'Iacobescu','Damaris',null,0720624034,'Farmacie',null,2018,2023,'calatorie,muzica,fotografii',1,null,50),
(42,'Pasca','Adrian','1999-12-20',0742592286,'UTCN - ETTI','Inginerie Economica',2018,2022,'sport,muzica,mers pe munte,citit si inca cateva :))',1,null,52),
(43,'Stanescu','Eduard','1994-10-03',0751388084,'Facultatea de Matematica si Informatica','Master Info (Sisteme Distributie in Internet)',2018,2020,'Sport, Citit',0,null,29),
(44,'Morariu','Brigita','1998-09-26',0747797909,'Facultatea de Psihologie si Stiinte ale Educatiei','Psihologie',2018,2021,'1. Stat in natura (drumetii, mers pe munte si prin padure)2. Arta (pictura murala, arhitectura, muzica, fotografie, teatru, poezie) 3. Calatorit ',0,null,42),
(45,'Dragota','Paul','1996-10-22',0758389138,'UCTN','ETTI',2015,2019,'Badminton, Ciclism, Culturism - sporturi in general',1,null,6);



#####################################
--           COMITET              --
#####################################

INSERT INTO comitet  VALUES 
(1,'2018-05-05','2019-05-10','Coordonarea Asociatiei Amicus Cluj'),
(2,'2018-11-10',null,'Coordonarea Proiectului Amicus Cuisine'),
(3,'2018-11-10',null,'Coordonarea dep. Social'),
(4,'2018-11-10',null,'Coordonarea dep. Proiecte'),
(5,'2018-11-10',null,'Coordonarea dep. Cultural');
#(4,'','',''),
#(,'','',''),



#####################################
--           IMPLICARE              --
#####################################

INSERT INTO implicare  VALUES
# idImplicare,idComitet, idVoluntar,idAdmin 
# COMITETUL ORGANIZATIEI
(1,1,null,1001),
(2,1,null,1002),
(3,1,null,1003),
(4,1,null,1004),
(5,1,null,1005),
(6,1,null,1006),
(7,1,null,1007),
(8,1,null,1008),
(9,1,null,1009),
(10,1,null,1015),
#COMITETUL AMICUS CUISINE
(11,2,13,null), -- Dina
(12,2,null,null), -- Odette
(13,2,null,1015), -- Rona
#COMITET dep. SOCIAL
(14,3,null,1005),
(15,3,34,null),
(16,3,28,null),
(17,3,33,null),
#COMITET dep. PROIECTE
(18,4,null,1015),
(19,4,17,null),  -- Mada Grigoras
#COMITET dep. CULTURAL
(20,5,null,1001), -- Mada C
(21,5,37,null), -- Stef C
(22,5,38,null),  -- Andreea G
(23,5,23,null);  -- Emi O, lipseste Mada Gocica
-- -------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------



#####################################
--           LOCATIE              --
#####################################

INSERT INTO locatie VALUES
(1,'Cluj-Napoca','Calea Motilor', '47', 'Centru'),
(2,'Cluj-Napoca','Calea Motilor', '48', 'Sakura'),
(3,'Cluj-Napoca', 'o strada' , 'un numar', 'Parcul Central'),
(4,'Cluj-Napoca', 'o strada' , 'un numar', 'Diverta'),
(5,'Cluj-Napoca','Dionisie Roman','un nr', 'langa Leroy Merlin'),
(6,'Cluj-Napoca','o strada' , 'un numar', 'Iulius Mall'),
(7,'Ciurila','o strada' , 'un numar','Scoala'),
(8,'Vama','o strada' , 'un numar','Moldova'),
(9,'Cluj-Napoca','o strada' , 'un numar','Sala de conferinte Cluj Arena'),
(10,'Cluj-Napoca', 'Streiului','3','Dynamis'),
(11,'Balan','o strada' , 'un numar','Harghita'),
(12,'Satic','o strada' , 'un numar','Oltenia'),
(13,'Cluj-Napoca','o strada' , 'un numar','Biserica Filadelfia'),
(14,'Timisoara','o strada' , 'un numar','o zona'),
(15,'Bucuresti','o strada' , 'un numar','o zona'),
(16,'Cluj-Napoca','o strada' , 'un numar','o zona'),
(17,'Amsterdam','o strada' , 'un numar','Europa');


#####################################
--       SPATII DEPOZITARE         --
#####################################

INSERT INTO spatiuDepozitare VALUES
('Debara',0,1),
('Garaj',200,5);  



#####################################
--              OBIECTE             --
#####################################

INSERT INTO obiect VALUES
########  in GARAJ ##########
('Pavilioane mici', '35-40', null, 'Garaj'),
('Pavilioane mari','2',null,'Garaj'),
('Trabmbulina','1',null,'Garaj'),
('Foi cloroate',null,null,'Garaj'),
('Foarfeci',null,null,'Garaj'),
('Bolduri',null,null,'Garaj'),
('Postituri',null,null,'Garaj'),
('Scoch',null,null,'Garaj'),
('Banda de hartie',null,null,'Garaj'),
('Dublu adeziv',null,null,'Garaj'),
('Capsatoare',null,null,'Garaj'),
('Servetele umede','25',null,'Garaj'),
('Fixative','10',null,'Garaj'),
('Placi de par','3',null,'Garaj'),
('Materiale pentru decor', null, null,'Garaj'),
('Tricouri',null,null,'Garaj'),
('Prelungitoare','10',null,'Garaj'),
########  in DEBARA  ##########
('Foi cloroateD',null,null,'Debara'),
('FoarfeciD',null,null,'Debara'),
('BolduriD',null,null,'Debara'),
('PostituriD',null,null,'Debara'),
('ScochD',null,null,'Debara'),
('Banda de hartieD',null,null,'Debara'),
('Dublu adezivD',null,null,'Debara'),
('CapsatoareD',null,null,'Debara'),
('Walkie-talkie','6',null, 'Debara'),
('PrelungitoareD','2',null,'Debara');
-- -------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------



#####################################
--      PROFIL ORGANIZATIE         --
#####################################


INSERT INTO profilOrganizatie VALUES
('Asociatia Studenteasca Amicus Cluj-Napoca','1993-10-01','Ne propunem sa stabilim relatii de prietenie in mediile studentesti si sa oferim oportunitati in dezvoltarea personala avand la baza principii de viata sanatoase pentru trup, minte si suflet.

Dorim ca impreuna sa venim in intampinarea nevoilor comunitatii prin actiuni de voluntariat si sa il descoperim pe Dumnezeu pentru o viata cu sens.

HAI SA FIM PRIETENI!
',1,'Iosana Ciutan','0744818933',1,24775110,'RO69BTRL01301205N60457XXs',null);  -- ultimul e idBuget



#####################################
--         DEPARTAMENT              --
#####################################

INSERT INTO departament(idDepartament,numeDepartament,idDirector)VALUES
(1,'Cultural',1001),
(2,'Social',1005),
(3,'Recreativ',1002),
(4,'Economic',1003),
(5,'PR',1004),
(6,'Proiecte',1015);

-- -------------------------------------------------------------------------------------------------------------------------------
-- -------------------------------------------------------------------------------------------------------------------------------



#####################################
--              ACTIVITATI               --
#####################################

INSERT INTO activitate VALUES
#idActiv idProiect idEventim deschire tipaActivitate enum (Afara, Inauntru', 'titlu'
(1,null,null,null,'Inauntru','Scaune muzicale'),
(2,null,null,null,'Inauntru','Vaduva Vesela'),
(3,null,null,' Da - se schimba intre ei. Nu - Dar pe cine iubesti?','Inauntru','Iti iubesti vecinii?'),
(4,null,null,' *de cerut expilatii','Afara','Incetul cu incetul se facbrica otetul'),
(5,null,null,null,'Inauntru','Mafiotii'),
(6,null,null,null,'Inauntru','Spionii'),
(7,null,null,' ','Inauntru','Contact 321'),
(8,null,null,null,'Inauntru','Sclavii'),
(9,null,null,null,'Inauntru','Impuls'),
(10,null,null,'Munte vs mare, rebel vs cuminte ','Inauntru','Una vs. Cealalta'),
(11,null,null,'Numele spune tot ','Inauntru','Scaunul adevarului'),
(12,null,null,'In grupe, trebuie sa cream o lume utopica/distopica','Inauntru','Lume distopica/utopica'),
(13,null,null,'Numele spune tot','Inauntru','Exercitii de dictie'),
(14,null,null,'','Inauntru','Lunile anului'),
(15,null,null,'Numele spune tot','Inauntru','Ce incape intr-o sticluta?'),
(16,null,null,' Stabilesti intalniri cu persoane pe care nu le cunosti al o anumita ora si discutati pe un subiect','Inauntru','Ceasul - de cunoastere'),
(17,null,null,' Grupuri de 5-10. Li se da o tema si trebuie sa faca o sceneta ','Inauntru','Piesa de teatru'),
(18,null,null,'Ti se da o suma de bani al inceput, apoi se citeste o lista cu (masina, casa, vacanta, familie etc) si te intreaba daca vrei sa cumperi sau nu (l-a facut Mada Gogica) ','Inauntru','Pe ce cheltuim banii?'),
(19,null,null,'Sunt 3 runde: 1. Descrii cuvantul prin cuvinte. 2. Descrii cuvantul cu un cuvant. 3. Mimezi cuvantul','Inauntru','Cuvinte'),
(20,null,null,'Li se da o imagine cu cat mai multe obiecte si 30 de s pt a scrie cat mai multe dintre ele','Inauntru','Memorie fotografica');





#####################################
--              PROIECT               --
#####################################

INSERT INTO proiect VALUES
#idProiect idDep idLocatie numeProiect descriere dataInceput dataFinal idComitet
(1,6,3,'Oraselul Copiilor','Proiectul Oraselul copiilor isi propune colaborarea cu diferite institutii ale statului pentru a atinge obiectivele enumerate mai sus. Fiecare institutie va avea un stand in cadrul caruia copiilor le va fi prezentat rolul acesteia (al institutiei) in societate, intr-un mod atractiv. Copiii vor vizita fiecare stand pentru a cunoaste fiecare institutie prezenta.','2018-06-01',null,null),
(2,6,3,'Oraselul Copiilor','Proiectul Oraselul copiilor isi propune colaborarea cu diferite institutii ale statului pentru a atinge obiectivele enumerate mai sus. Fiecare institutie va avea un stand in cadrul caruia copiilor le va fi prezentat rolul acesteia (al institutiei) in societate, intr-un mod atractiv. Copiii vor vizita fiecare stand pentru a cunoaste fiecare institutie prezenta.','2017-06-01',null,null),
(3,6,3,'Oraselul Copiilor','Proiectul Oraselul copiilor isi propune colaborarea cu diferite institutii ale statului pentru a atinge obiectivele enumerate mai sus. Fiecare institutie va avea un stand in cadrul caruia copiilor le va fi prezentat rolul acesteia (al institutiei) in societate, intr-un mod atractiv. Copiii vor vizita fiecare stand pentru a cunoaste fiecare institutie prezenta.','2016-06-01',null,null),
(4,6,6,'Mama de Oscar','Pentru ca mamele sunt speciale, merita sa fie apreciate','2016-03-08',null,null),
(5,6,6,'Mama de Oscar','Pentru ca mamele sunt speciale, merita sa fie apreciate','2017-03-08',null,null),
(6,6,6,'Gala Mamelor','Pentru ca mamele sunt speciale, merita sa fie apreciate','2018-03-08',null,null),
(7,3,16,'Amicus Fit','Promovam un stil de viata sanatos','2018-10-15','2018-05-19',null),
(8,6,2,'Amicus Cuisine - Bunatati de sarbatori','Promovam un stil de viata sanatos','2018-12-16',null,null),
(9,6,2,'Amicus Cuisine - Ce mananc in sesiune?','Promovam un stil de viata sanatos','2019-01-13',null,null),
(10,1,7,'Cine da o carte, Bucurie imparte','Copiii din Ciurila vor avea o biblioteca mai bogata din 12 decembrie','2018-12-04','2018-12-12',null),
(11,1,4,'Clubul de Lectura','Eu sunt Malala','2018-05-08',null,null),
(12,1,4,'Clubul de Lectura','Europa in 80 de zile','2018-05-29',null,null),
(13,1,4,'Clubul de Lectura','Secretele succesului','2018-10-30',null,null),
(14,1,2,'Clubul de Lectura','Evadare din lagarul 14','2018-12-04',null,null),
(15,1,2,'Seara de poezii','Poezia este arta, iar modul in care reusesc cei care scriu, in putine sau mai multe cuvinte sa trasmita emotii, sa planteze un gand, sa faca sa schimbe perspective sau sa smulga lacrimi de fericire este unul cu totul aparte.','2018-12-11',null,null),  -- locati 2 - Sakura, 4 - diverta
(16,3,16,'Amicus Fit','Promovam un stil de viata sanatos','2019-10-15','2020-05-19',null),
(17,3,16,'Amicus Fit','Promovam un stil de viata sanatos','2020-10-15','2021-05-19',null),
(18,3,16,'Amicus Fit','Promovam un stil de viata sanatos','2021-10-15','2022-05-19',null),
(19,3,16,'Amicus Fit','Promovam un stil de viata sanatos','2022-10-15','2023-05-19',null);
/*
(20,,,'','','','',),
(21,,,'','','','',),
(22,,,'','','','',),
(23,,,'','','','',),
(24,,,'','','','',),
(25,,,'','','','',),
(26,,,'','','','',),
(27,,,'','','','',);
*/



#####################################
--                 STAND             --
#####################################

INSERT INTO stand(idstand,numeStand,idProiect) VALUES
#idStand nume descriereActiv responsS nrScaune areStampila materialeNevesare bugetEstimat nr.Vlountar idProiect
(1,'Primarie',1),
(2,'Politie',1),
(3,'Jandarmerie',1),
(4,'Pompieri',1),
(5,'SMURD',1),
(6,'Judecatorie',1),
(7,'Sppital',1),
(8,'Posta',1),
(9,'Biserica',1),
(10,'Teatru',1),
(11,'Muzeu',1),
(12,'Filarmonica',1),
(13,'Circ',1),
(14,'Piata',1),
(15,'Atelier de meserii',1),
(16,'Frizerie-Coafor',1),
(17,'Centru de calcul',1),
(18,'Centru foto',1),
(19,'Televiziune',1),
(20,'Exploratori',1),
(21,'Parc de Aventura',1),
(22,'Gradinita',1),
(23,'Baza Sportiva',1),
(24,'Aerodinamica',1),
(25,'Birou Contabilitate',1);




#####################################
--             EVENIMENT             --
#####################################

INSERT INTO eveniment VALUES
# idEveniment idLocatie idDepartament idComitet numeEv  dataInc dataSf oraIncep oraSf tipEv('Intalnire Amicus', 'Bobociada', 'Absolvire', 'Serata', 'Revelion', 'Excursie in afara', 'Vizita') NULL DEFAULT NULL,
(1,1,6,null,'Voluntariat','2018-12-12','2018-12-12','19:00:00','22:00:00','Intalnire Amicus'), -- la comitet, Dani si Ale
/*(2,1,6,null,'Cina cu prietenii','2018-12-12',null,'20:00:00','23:00:00','Intalnire Amicus'),
(3,1,6,null,'Cina cu prietenii','2017-12-13',null,'20:00:00','23:00:00','Intalnire Amicus'),
(4,1,6,null,'Dumnezeu: gasit sau mostenit','2018-12-05',null,'20:00:00','23:00:00','Intalnire Amicus'),
(5,1,6,null,'Sensul libertatii','2018-11-28',null,'20:00:00','23:00:00','Intalnire Amicus'),
(6,1,6,null,'Planifica!','2018-12-21',null,'20:00:00','23:00:00','Intalnire Amicus'),
(7,1,6,null,'De la sedentar la mii de kilometrii','2018-11-14',null,'20:00:00','23:00:00','Intalnire Amicus'),
(8,1,6,null,'Vulnerabilitatea: de la slabiciune la putere','2018-11-7',null,'20:00:00','23:00:00','Intalnire Amicus'),
(9,1,6,null,'Fundamentalismul politic si religios','2018-10-31',null,'20:00:00','23:00:00','Intalnire Amicus'),
(10,1,6,null,'Sileste-i sa se bucure','2018-10-24',null,'20:00:00','23:00:00','Intalnire Amicus'),
(11,3,6,null,'Amicusii sunt inca frumosi','2017-05-19',null,'20:00:00','23:00:00','Intalnire Amicus'),  -- Amicusii sunt inca frumosi
(12,9,6,null,'Inteligenta Emotionala','2018-06-12',null,'19:00:00','22:00:00','Intalnire Amicus'),   -- Zoltan Veres
(13,10,6,null,'Serata','2018-10-13','2018-10-14','21:00:00','01:00:00','Serata'),
(14,10,6,null,'Serata bobociada','2018-10-27','2018-10-28','21:00:00','01:00:00','Serata'),
(15,1,6,null,'Eu,Tu si Noi','2018-10-27',null,'16:00:00','18:00:00','Bobociada'),  -- 2018
(16,13,6,null,'Fara Bariere','2017-10-21',null,'16:00:00','18:00:00','Bobociada'),  -- 2017
(17,1,6,null,'Introspectie','2018-06-16',null,'17:00:00','19:00:00','Absolvire'),
(18,1,6,null,'Viata din plin','2017-06-17',null,'17:00:00','19:00:00','Absolvire'),
(19,1,6,null,null,'2019-06-15',null,'17:00:00','19:00:00','Absolvire'),
(20,17,6,null,'Excursie in Spnia','2018-04-04','2018-04-13',null,null,'Excursie in afara'),  -- 2018
(21,17,6,null,'Excursie in Grecia','2017-04-11','2017-04-20',null,null,'Excursie in afara'),  -- 2017
(22,11,6,null,'Avant','2018-12-30','2019-01-02','18:00:00','18:00:00','Revelion'),   -- 2019
(23,12,6,null,'Amanunt','2017-12-29','2018-01-02','18:00:00','12:00:00','Revelion'),  -- 2019
(24,8,6,null,'Vamalion','2016-12-30','2017-01-02',null,null,'Revelion'),  -- ?
(25,1,6,null,'Din toata inima','2017-11-11','2020-12-12',null,null,'Vizita'),    -- Chisinau
(26,1,6,null,'Reper','2018-03-24',null,null,null,'Vizita'),    -- Iasi  
(27,14,6,null,null,null,null,null,null,'Vizita'),  -- Timisoara
(28,15,6,null,null,null,null,null,null,'Vizita'),  -- Bucuresti*/
(29,1,6,null,'Cina cu prietenii','2019-12-12','2019-12-12','20:00:00','23:00:00','Intalnire Amicus'),
(30,1,6,null,'Cina cu prietenii','2020-12-12','2020-12-12','20:00:00','23:00:00','Intalnire Amicus'),
(31,1,6,null,'Ca pe tine insuti','2019-01-01','2019-01-01','20:00:00','23:00:00','Intalnire Amicus'),
(32,1,6,null,'Moralitate si constiinta','2019-02-02','2019-02-02','20:00:00','23:00:00','Intalnire Amicus'),
(33,13,6,null,'Fara Prejudecati','2019-06-06','2019-06-06','16:00:00','18:00:00','Bobociada');



#####################################
--            INVITATI            --   -- trebuie pusa dupa evenimente, la fel si participant
#####################################

INSERT INTO invitat VALUES
# idInvitat idEveniment idProiect nume prenume oras job
(1,NULL,6,'Pandea','Bogdan','Cluj-Napoca','CEO - CaptainBean'),
(2,NULL,NULL,'Poenariu','Gelu','Tg-Mures','pastor'),
(3,NULL,8,'Poenariu','Nita',' ','CEO - CaptainBean'),
(4,NULL,9,'Thomas','Daniel','Brasov ','pastor'),
(5,NULL,1, 'Voiculescu','Vlad','Bucuresti','fondator MagicCamp'),
(6,NULL,10,'Dorgo','Adi','Sighet','pastor'),
(7,NULL,NULL,'Pasca','Iosif','Cluj','pastor'),
(8,NULL,NULL,'Bursuc','Cornel','Pascani','pastor'),
(9,NULL,NULL,'Feier','Ionut','Cluj','pastor'),
(10,NULL,NULL,'Miclea','Mircea',null,'pastor'),
(11,NULL,NULL,'Sisu','Beni','Craiova','pastor'),
(12,NULL,NULL,'Mitica','Claudiu','Slatina','pastor'),
(13,NULL,NULL,'Andrei','Marius',null,'pastor'),
(14,NULL,NULL,'Munteanu','Marius',null,'pastor'),
(15,NULL,NULL,'Gasman','Claudiu',null,null),
(16,NULL,NULL,'& Co','Gina','Cluj','lucreaza in Politie'),
(17,NULL,NULL,'Useriu','Tibi',null,'sportiv'),
(18,NULL,7,'Matei','Bogdan','Brasov','sportiv'),
(19,NULL,NULL,'Melania','Medeleanu','Bucuresti','Fondator MagiCAMP'),
(20,NULL,NULL,'Iarca','Gabi','Bucuresti','Coordonator Respiro'),
(21,NULL,NULL,'Rosu','Andrei',null,null),
(22,NULL,NULL,'Pop','Oana','Cluj ','Prof. la USAMV'),
(23,NULL,NULL,'Ciurea','Natalia','Slatina','Profesoara'),
(24,NULL,NULL,'Riz','Oana',null,'Instagramer'),
(25,NULL,NULL,'Norel','Iacob','Bucuresti ','pastor'),
(26,NULL,NULL,'Popescu','Cristian-Tudor','Bucuresti','jurnalist'),
(27,NULL,12,'Veres','Zoltan','Cluj','public speaker');


-- -------------------------------------------------------------------------------------------------------------------------------
# BUGET #

insert into buget values(1,100,0,0);	#Cultural
insert into buget values(2,100,0,0);	#Social
insert into buget values(3,100,0,0);    #Recreativ
insert into buget values(4,100,0,0);	#Economic
insert into buget values(5,100,0,0);	#PR
insert into buget values(6,100,0,0);	#Proiecte
insert into buget values(7,100,0,0);	#Amicus	
insert into buget values(8,700,0,0); 	#profil


# update departamente si profil cu buget #

update departament set idBuget=1 where idDepartament = 1;
update departament set idBuget=2 where idDepartament = 2;
update departament set idBuget=3 where idDepartament = 3;
update departament set idBuget=4 where idDepartament = 4;
update departament set idBuget=5 where idDepartament = 5;
update departament set idBuget=6 where idDepartament = 6;
update departament set idBuget=7 where idDepartament = 7;
update profilorganizatie set idbuget=8 where nume='Asociatia Studenteasca Amicus Cluj-Napoca';


/*

#####################################
--              PARTICIPANTI                --
#####################################

INSERT INTO participant VALUES
();

*/

/*

#####################################
--              SPONSOR                --
#####################################

INSERT INTO sponsor(idSponsor,companie) VALUES
#idSponsor nume prenume functie companie idUser
(1,'Biblioteca Judeteana "Ocatvian Goga'),
(2,'Muzeul National de Istorie'),
(3,'SanoVita'),
(4,'MD Dental Junior'),
(5,'New York Dental'),
(6,'Learn with Music'),
(7,'Kaufland'),
(8,'Diverta');
-- OBSERVATIE:  idUser la sponsor nu poate sa fie null, de asta l-am comentat





#####################################
--              FINANTARE                --
#####################################

INSERT INTO finantare VALUES
();








#####################################
--              FACTURA            --
#####################################

INSERT INTO factura VALUES
();





#####################################
--              BUGET            --
#####################################

INSERT INTO buget VALUES
();

*/













