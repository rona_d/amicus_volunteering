USE `amicus`;
DROP procedure IF EXISTS `get_total_evenimente`;

DELIMITER $$
USE `amicus`$$
CREATE PROCEDURE `get_total_evenimente` (out output int , in idSponsor1 int)
BEGIN
	select  total_donatii_eveniment(idSponsor1) into output;
END$$

DELIMITER ;




USE `amicus`;
DROP procedure IF EXISTS `get_total`;

DELIMITER $$
USE `amicus`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_total`(
	out output int , in idSponsor1 int
)
begin
	
   
    
	select  total_donatii(idSponsor1) into output;
   

end$$

DELIMITER ;



USE `amicus`;
DROP procedure IF EXISTS `get_total_proiecte`;

DELIMITER $$
USE `amicus`$$
CREATE PROCEDURE `get_total_proiecte` ( out output int , in idSponsor1 int)
BEGIN
	select  total_donatii_proiect(idSponsor1) into output;
END$$

DELIMITER ;


