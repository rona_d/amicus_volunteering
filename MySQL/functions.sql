USE `amicus`;
DROP function IF EXISTS `total_donatii_proiect`;

DELIMITER $$
USE `amicus`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_donatii_proiect`( idSponsor1 int) RETURNS int(11)
    DETERMINISTIC
begin

    declare suma1 int default 0;
    
    select sum(suma+sumabonuri) into suma1 from factura inner join finantare using (idfinantare) 
		inner join sponsor using (idsponsor) where idsponsor=idSponsor1 and idEveniment is null; 
    
	return suma1;
    
    
end$$

DELIMITER ;

USE `amicus`;
DROP function IF EXISTS `total_donatii_eveniment`;

DELIMITER $$
USE `amicus`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_donatii_eveniment`( idSponsor1 int) RETURNS int(11)
    DETERMINISTIC
begin

    declare suma1 int default 0;
    
    select sum(suma+sumabonuri) into suma1 from factura inner join finantare using (idfinantare) 
		inner join sponsor using (idsponsor) where idsponsor=idSponsor1 and idProiect is null; 
    
	return suma1;
    
    
end$$

DELIMITER ;

USE `amicus`;
DROP function IF EXISTS `total_donatii`;

DELIMITER $$
USE `amicus`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `total_donatii`( idSponsor1 int) RETURNS int(11)
    DETERMINISTIC
begin
	
    
    
    declare suma1 int default 0;
    
    select sum(suma+sumabonuri) into suma1 from factura inner join finantare using (idfinantare) 
		inner join sponsor using (idsponsor) where idsponsor=idSponsor1; 
    
	return suma1;
    
    
end$$

DELIMITER ;