
use amicus;
drop procedure if exists tranzactie_cont;

delimiter #

create procedure tranzactie_cont
(
	in suma1 int,  in sumaBonuri1 int, in iBAN_sponsor varchar(30)
) 

begin
	
    declare idBug int default 0;
    select idBuget into idBug from profilorganizatie;
	
    update buget set sumaCont= sumaCont + suma1 + sumaBonuri1 where idBuget=idBug;
    update buget set sumaDonatii = sumaDonatii + suma1 where idBuget=idBug;
    update buget set valoareaBonurilor = valoareaBonurilor + sumaBonuri1 where idBuget=idBug;

end# -- end of stored procedure block

delimiter ; -- switch delimiters again


use amicus;
drop procedure if exists finantare_proiect;

delimiter #

create procedure finantare_proiect
(
	in idProiect1 int, in idSponsor1 int, in suma1 int , in sumaBonuri1 int
) 

begin
	declare ibanSpons varchar(30) default 'null';
	declare idDep int default 0;
    declare idBug int default 0;
	declare insertedIdFinantare int default 0;
    
   
    select iBAN into ibanSpons from sponsor where idSponsor=idSponsor1; 
    
    select idDepartament into idDep from proiect where idProiect=idProiect1;
    select idBuget into idBug from departament where idDepartament=idDep;
	
    
    insert into finantare(idProiect,idSponsor) values (idProiect1,idSponsor1);
    select max(idFinantare) into insertedIdFinantare from finantare;
    
	insert into factura(dataEmitere,oraEmitere,sumaBonuri,suma,idFinantare) values (now(),current_time(),sumaBonuri1,suma1,insertedIdFinantare);
    
    update buget set sumaCont= sumaCont + suma1 + sumaBonuri1 where idBuget=idBug;
    update buget set sumaDonatii = sumaDonatii + suma1 where idBuget=idBug;
    update buget set valoareaBonurilor = valoareaBonurilor + sumaBonuri1 where idBuget=idBug;
    
    call tranzactie_cont(suma1,sumaBonuri1,ibanSpons);

end# -- end of stored procedure block

delimiter ; -- switch delimiters again




drop procedure if exists finantare_eveniment;

delimiter #

create procedure finantare_eveniment
(
	in idEveniment1 int, in idSponsor1 int, in suma1 int , in sumaBonuri1 int
) 

begin
	declare ibanSpons varchar(30) default 'null';
	declare idDep int default 0;
    declare idBug int default 0;
	declare insertedIdFinantare int default 0;
    
    select iBAN into ibanSpons from sponsor where idSponsor=idSponsor1; 
    select idDepartament into idDep from eveniment where idEveniment=idEveniment1;
    select idBuget into idBug from departament where idDepartament=idDep;
	
    
    insert into finantare(idEveniment,idSponsor) values (idEveniment1,idSponsor1);
    select max(idFinantare) into insertedIdFinantare from finantare;
    
	insert into factura(dataEmitere,oraEmitere,sumaBonuri,suma,idFinantare) values (now(),current_time(),sumaBonuri1,suma1,insertedIdFinantare);
    
    update buget set sumaCont= sumaCont + suma1 + sumaBonuri1 where idBuget=idBug;
    update buget set sumaDonatii = sumaDonatii + suma1 where idBuget=idBug;
    update buget set valoareaBonurilor = valoareaBonurilor + sumaBonuri1 where idBuget=idBug;
    
    call tranzactie_cont(suma1,sumaBonuri1,ibanSpons);

end# -- end of stored procedure block

delimiter ; -- switch delimiters again




use amicus;
drop procedure if exists insert_user;

delimiter #

create procedure insert_user 
(
in username1 varchar(20),in parola1 varchar(15),in tipUser1 enum('voluntarAmicus','sponsor','administrator'),in enabled1 bit(1),in email1 varchar(45)
) 
begin
insert into user(username,parola,tipUser,enabled,email) values(username1,parola1,tipUser1,enabled1,email1);
end# -- end of stored procedure block

delimiter ; -- switch delimiters again





use amicus;
drop procedure if exists insert_voluntar33;

delimiter //

create procedure insert_voluntar33
(
in nume1 varchar(20),in prenume1 varchar(20),dataNasterii1 date, in nrTelefon1 varchar(13), anIntrareFacultate1 year, anAbsolvireFacultate1 year,in statusCor1 enum('sopran','alto','bas','tenor','nu'),numeFacultate1 varchar(40),specializareFacultate1 varchar(30),hobbyuri varchar(220), in statusBotezat int )
 
begin
#declare statusB int default 0;
declare insertedId int default 0;
select max(idUser) into insertedId from user;

#if(Year(dataNasterii1) >= 18) then
#set statusB = 1;
#end if;

insert into voluntaramicus(nume,prenume,dataNasterii,nrTelefon,numeFacultate,specializareFacultate,anIntrareFacultate,anAbsolvireFacultate,hobbyuri,statusBotezat,statusCor,idUser)
values(nume1,prenume1,dataNasterii1,nrTelefon1,numeFacultate1,specializareFacultate1,anIntrareFacultate1,anAbsolvireFacultate1,hobbyuri,statusBotezat,statusCor1,insertedId);


end//-- end of stored procedure block




drop procedure if exists delete_last2;

delimiter #
create PROCEDURE `delete_last2`(
)
begin
delete from user
order by idUser desc limit 1;
end#
delimiter ;







drop procedure if exists admin_sign_up;

delimiter #

create procedure admin_sign_up 
(
nume1 varchar(45),prenume1 varchar(45),dataNasterii1 date,numeFacultate1 varchar(40),specializareFacultate1 varchar(30),nrTelefon1 varchar(11),anIntrareFacultate1 year(4),anAbsolvireFacultate1 year(4),statusCor1 enum('sopran','alto','tenor','bas','nu'),functie1 varchar(50),codValidare int(11)
) 
begin
declare insertedId int default 0;
select max(idUser) into insertedId from user;


update administrator set nume = nume1,prenume = prenume1,idUser = insertedId,dataNasterii = dataNasterii1,numeFacultate = numeFacultate1,specializareFacultate = specializareFacultate1,nrTelefon = nrTelefon1,anIntrareFacultate = anIntrareFacultate1,anAbsolvireFacultate=anAbsolvireFacultate1,statusCor = statusCor1,functie = functie1 where idAdmin = codValidare;

end# -- end of stored procedure block

delimiter ; -- switch delimiters again







use amicus ;
drop procedure if exists insert_sponsor;
delimiter //
create procedure insert_sponsor
(
in nume1 varchar(45),in prenume1 varchar(20),in functie1 varchar(45),in companie1 varchar(45), in iBAN1 varchar(30) )
 
begin

declare insertedId int default 0;
select max(idUser) into insertedId from user;


insert into sponsor(nume,prenume,functie,companie,idUser,iBAN) values(nume1,prenume1,functie1,companie1,insertedId,iBAN1);

end//
delimiter ;

use amicus;
drop procedure if exists participare_proc;

delimiter #

create procedure participare_proc
(
	in idUser1 int,  in idEveniment1 int, out result int
) 

begin	
  
    declare id int default 0 ;
	
    set result = 0;
    
    
    SELECT  idParticipant into id
    FROM participant
    WHERE idUser = idUser1  AND idEveniment = idEveniment1;
    
    if id = 0  then
    
    set result = 1;
    INSERT INTO participant(idUser,  idEveniment) 
    VALUES (idUser1, idEveniment1);
   end if; 
    
	        
end# -- end of stored procedure block

delimiter ; -- switch delimiters again

